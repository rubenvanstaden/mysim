"""

Usage:
    josim <testname>
    josim (-h | --help)
    josim (-V | --version)

Options:
    -h --help     Show this screen.
    -p --pretty   Prettify the output.
    -V --version  Show version.
    --quiet       print less text
    --verbose     print more text

"""


from docopt import docopt
from scipy.linalg import solve
import matplotlib.pyplot as plt

import collections
import tools
import os
import subprocess
import networkx as nx
import numpy as np
import stamps
import json


"""
Hacker: 1h3d*n
For: Volundr
Docs: Algorithm 1
Date: 31 April 2017

Description: Morph the moat layer and the wire layers.

1) Get a list of all the polygons inside the GDS file.
2) Send this list to the Clip library with the wiring
   layer number and the moat layer number as parameters.
3) Get the union of all the wiring layer polygons that
   are connected. Update this to check for vias.
4) Get the intersection of the moat layer with the
   wiring layer and save this in a new polygon.
5) Get the difference of the moat layer with the
   wiring layer and save this in a new polygon.
6) Join the intersected and difference polygons
   to form a list of atleast 3 polygons.
7) We now know which part of the wiring layer
   goes over the moat is most probably mutually
   connected to wiring layer 2.
8) Send this polygon structure to GMSH.
"""


# class myMat:
#     def __init__(self, mat, name):
#         self.mat = mat
#         self.name = name
#         self.head = ['a','b','c','d','e','f']
#         self.sep = ', '
# 
#     def __str__(self):
#         s = "%s%s"%(self.name, self.sep)
#         for x in self.head:
#             s += "%s%s"%(x, self.sep)
#         s = s[:-len(self.sep)] + '\n'
# 
#         for i in range(len(self.mat)):
#             row = self.mat[i]
#             s += "%s%s"%(self.head[i], self.sep)
#             for x in row:
#                 s += "%s%s"%(str(x), self.sep)
#             s += '\n'
#         s = s[:-len(self.sep)-len('\n')]
# 
#         return s


# class Element():
# 
#     def __init__(self, name, lut_row=[], low_col=[], mat=[]):
#         self.name = name
#         self.lut_row = lut_row
#         self.lut_col = lut_col
#         self.mat = np.array(mat)
# 
#     def get_mat_entry(self, i, j):
#         """
#             element : Is the value inside the stamp network.
#             key : Is a tuple that has in row/col indices of the element in the A-matrix, (i,j).
#         """
#         element = self.mat[i][j]
#         key = (self.lut_row[i], self.lut_col[j])
# 
#         return key, element


# def add_L_to_A(A, b, L, lut):
#     for i, col in enumerate(lut):
#         for j, row in enumerate(lut):
#             A[col][row] = L[i][j]
# 
#         b[col] = L[i][3]
# 
# 
# def add_Va_to_A(A, b, Va, lut):
#     for i, col in enumerate(lut):
#         for j, row in enumerate(lut):
#             A[col][row] = Va[i][j]
# 
#         b[col] = Va[i][2]
        
        
def read_config(config_file):
    """ Reads the config file that is written in
    JSON. This file contains the logic of how
    the different layers will interact. """

    data = None
    with open(config_file) as data_file:
        data = json.load(data_file)
    return data
    
    
def update_master_matrix(A, b, L, nodes, num):
    for i, col in enumerate(nodes):
        for j, row in enumerate(nodes):
            A[col][row] = L[i][j]

        if num == 3:
            b[col] = L[i][3]
        elif num == 2:
            b[col] = L[i][2]
        elif num == 1:
            b[col] = L[i][1]
        
    
def solve_matrices(A, b, x, dv, x_n5, t):
    h = 0.25e-12
    
    x_1 = x
    
    x[:] = solve(A, b)
    
    avn = x[1]
    avn_1 = x_1[1]
    adv_1 = dv[0]
    dv[0] = (2/h) * (avn - avn_1) - adv_1
    
    bvn = x[3]
    bvn_1 = x_1[3]
    bdv_1 = dv[1]
    dv[1] = (2/h) * (bvn - bvn_1) - bdv_1
    
    x_n5[t] = x[3]
    

def main():
    """  """

    args = docopt(__doc__, version='JoSIM 0.0.1')
    tools.red_print('Summoning JoSIM...')
    tools.parameter_print(args) 
    
    config_file = os.getcwd() + '/josim/' + 'config.json'
    netlist = read_config(config_file)

    size = 14

    A = np.zeros((size, size))
    
    x = np.zeros(size)
    x_1 = np.zeros(size)

    jj_num = 2
    dv = np.zeros(jj_num)

    T = 1000e-12
    h = 0.25e-12
    
    LoopSize = int(T/h)
    Time = np.arange(0, LoopSize, 1)

    x_n5 = np.zeros(LoopSize)
    a = np.zeros((LoopSize,))
    Ib = np.zeros((LoopSize,))
    b = np.zeros(size)
    
    Vm = 827e-6
    Im = 200e-6
    
    for i in range(1000, 1010, 1):
        a[i] = (Vm/10) * (i-1000)
    for i in range(1010, 1020, 1):
        a[i] = Vm - ((Vm/10) * (i-1010))
        
    for i in range(0, 20, 1):
        Ib[i] = (Im/20) * (i)
    for i in range(20, LoopSize, 1):
        Ib[i] = Im
        
    for t in Time:
        n = 0
        for key, element in netlist['Elements'].items():
            submatrix = None
            num = 0
    
            if element['type'] == 'V':
                submatrix = stamps.init_Va(a[t], element['nodes'])
                num = 2
            elif element['type'] == 'R':
                submatrix = stamps.calc_R1(element['nodes'])
                num = 1
            elif element['type'] == 'L':
                if element['dir'] == 'pos':
                    submatrix = stamps.calculate_inductance(x, element, 1, -1, 0) 
                else:
                    submatrix = stamps.calculate_inductance(x, element, -1, 1, Ib[t])
                num = 3
            elif element['type'] == 'B':    
                submatrix = stamps.calculate_junction(x, x_1, element, dv, n)
                n += 1
                num = 2
    
            if submatrix is not None:
                update_master_matrix(A, b, submatrix, element['nodes'], num)
    
        solve_matrices(A, b, x, dv, x_n5, t)
        
    plt.subplot(212)
    plt.plot(Time, x_n5, 'r-')
    plt.show()
        
    tools.red_print('Auron. Done.')
    

if __name__ == '__main__':
    main()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
