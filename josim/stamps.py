import numpy as np


def calculate_inductance(x, element, n1, n2, Ib):
    h = 0.25e-12
    
    nodes = element['nodes']
    Ln = element['value']
    
    br = [0, 0, 0, 0]

    br[0] = n1
    br[1] = n2
    br[2] = -2*Ln/h

    Vp1 = x[nodes[0]]
    Vp2 = x[nodes[1]]
    Ip = x[nodes[2]]

    br[3] = (br[2] * Ip) + Vp2 - Vp1

    matrix = [[    0,     0,   n1,     0],
              [    0,     0,   n2,     Ib],
              [br[0], br[1], br[2], br[3]]]

    return matrix


def calculate_junction(x, x_1, element, dv, n):
    h = 0.25e-12
    C = 2.16*0.07e-12
    R0 = 160.0
    Rn = 16.0
    Phi_0 = 2.06783383113e-15
    egap = 2.0*np.pi/Phi_0
    
    nodes = element['nodes']
    
    vn_1 = x[nodes[0]]
    phi_1 = x[nodes[1]]
    
    Is = calculate_Is(vn_1, phi_1, dv[n])

    br = [0, 0, 0]

    br[0] = (-h/2.0) * egap
    br[1] = 1.0
    br[2] = phi_1 + ((h/2.0) * egap * vn_1)

    BN2 = [0, 0, 0]

    BN2[0] = ((2.0*C)/h) + 2.16*(1.0/R0) + (1.0/3.0)
    # BN2[0] = ((2.0*C)/h) + (1.0/(R0))
    BN2[1] = 0
    BN2[2] = Is

    matrix = [[BN2[0], BN2[1], BN2[2]],
              [ br[0], br[1], br[2]]]

    return matrix
    
    
def calculate_Is(vn_1, phi_1, dv_1):
    h = 0.25e-12
    # C = 0.07e-12
    C = 2.16*0.07e-12
    Ic = 100e-6
    Phi_0 = 2.06783383113e-15
    egap = 2.0 * np.pi/Phi_0

    vn_0 = vn_1 + dv_1*h

    phi_n = phi_1 + (h/2.0) * egap * (vn_1 + vn_0)
    
    Is = -Ic * np.sin(phi_n) + ((2.0*C)/h)*vn_1 + C*dv_1
    
    return Is


def init_Va(a, lut):
    h = 0.25e-12

    br = [1.0, 0, a]
    
    matrix = [[0,       1.0,     0], 
              [br[0], br[1], br[2]]]
              
    return matrix
    
    
def calc_R1(lut):
    R1 = 2.0
    
    br = [0, 0]

    br[0] = 1.0/R1
    br[1] = 0.0

    # matrix = [[0,       1.0,     0],
    #           [br[0], br[1], br[2]]]

    matrix = [[br[0], br[1]]]
              
    return matrix
    

def calc_L1(x, t, lut):
    # h = 0.25e-12
    h = 0.1e-12
    L1 = 2.4e-12

    br = [0, 0, 0, 0]

    br[0] = 1
    br[1] = -1
    br[2] = -2*L1/h

    Vp1 = x[lut[0]]
    Vp2 = x[lut[1]]
    Ip = x[lut[2]]

    br[3] = br[2] * Ip - (Vp2 - Vp1)

    L1_mat = [[0,         0,   1.0,     0],
              [0,         0,  -1.0,     0],
              [br[0], br[1], br[2], br[3]]]

    return L1_mat


def calc_L2(x, t, lut):
    # h = 0.25e-12
    h = 0.1e-12
    L1 = 2.4e-12

    br = [0, 0, 0, 0]

    br[0] = 1
    br[1] = -1
    br[2] = -2*L1/h

    Vp1 = x[lut[0]]
    Vp2 = x[lut[1]]
    Ip = x[lut[2]]

    br[3] = br[2] * Ip - (Vp2 - Vp1)

    L1_mat = [[0,         0,   1.0,     0],
              [0,         0,  -1.0,     0],
              [br[0], br[1], br[2], br[3]]]

    return L1_mat


def calc_L3(x, t, lut):
    # h = 0.25e-12
    h = 0.1e-12
    L1 = 2.4e-12

    br = [0, 0, 0, 0]

    br[0] = 1
    br[1] = -1
    br[2] = -2*L1/h

    Vp1 = x[lut[0]]
    Vp2 = x[lut[1]]
    Ip = x[lut[2]]

    br[3] = br[2] * Ip - (Vp2 - Vp1)

    L1_mat = [[0,         0,   1.0,     0],
              [0,         0,  -1.0,     0],
              [br[0], br[1], br[2], br[3]]]

    return L1_mat


def calc_L4(x, t, lut):
    # h = 0.25e-12
    h = 0.1e-12
    L1 = 2.4e-12

    br = [0, 0, 0, 0]

    br[0] = 1
    br[1] = -1
    br[2] = -2*L1/h

    Vp1 = x[lut[0]]
    Vp2 = x[lut[1]]
    Ip = x[lut[2]]

    br[3] = br[2] * Ip - (Vp2 - Vp1)

    L1_mat = [[0,         0,   1.0,     0],
              [0,         0,  -1.0,     0],
              [br[0], br[1], br[2], br[3]]]

    return L1_mat


def calc_L5(x, t, lut):
    # h = 0.25e-12
    h = 0.1e-12
    L1 = 0.27e-12

    br = [0, 0, 0, 0]

    br[0] = 1
    br[1] = -1
    br[2] = -2*L1/h

    Vp1 = x[lut[0]]
    Vp2 = x[lut[1]]
    Ip = x[lut[2]]

    br[3] = br[2] * Ip - (Vp2 - Vp1)

    L1_mat = [[0,         0,   1.0,     0],
              [0,         0,  -1.0,     0],
              [br[0], br[1], br[2], br[3]]]

    return L1_mat
    
    
def calc_B1(x, x_1, t, lut, dv):
    # h = 0.25e-12
    h = 0.1e-12
    C = 0.07e-12
    R0 = 160
    Rn = 16
    Phi_0 = 2.06783383113e-15
    egap = 2.0*np.pi/Phi_0
    
    vn_1 = x[lut[0]]
    phi_1 = x[lut[1]]

    Is = calculate_Is(vn_1, phi_1, dv[0])

    br = [0, 0, 0, 0]

    br[0] = ((2.0*C)/h) + (1.0/R0)
    br[1] = 0
    br[2] = -1.0
    br[3] = Is

    phi = [0, 0, 0, 0]

    phi[0] = (-h/2.0) * egap
    phi[1] = 1.0
    phi[2] = 0
    phi[3] = phi_1 + ((h/2.0) * egap * vn_1)

    L1_mat = [[     0,      0,    1.0,      0],
              [phi[0], phi[1], phi[2], phi[3]],
              [ br[0],  br[1],  br[2],  br[3]]]

    return L1_mat


def calc_B2(x, x_1, t, lut, dv):
    # h = 0.25e-12
    h = 0.1e-12
    C = 0.07e-12
    R0 = 160
    Rn = 16
    Phi_0 = 2.06783383113e-15
    egap = 2*np.pi/Phi_0
    
    vn_1 = x[lut[0]]
    phi_1 = x[lut[1]]
    
    # vn_1 = x_1[lut[0]]
    
    Is = calculate_Is(vn_1, phi_1, dv[1])

    br = [0, 0, 0, 0]

    br[0] = ((2.0*C)/h) + (1.0/R0)
    br[1] = 0
    br[2] = -1.0
    br[3] = Is

    phi = [0, 0, 0, 0]

    phi[0] = (-h/2.0) * egap
    phi[1] = 1.0
    phi[2] = 0
    phi[3] = phi_1 + ((h/2.0) * egap * vn_1)

    L1_mat = [[     0,      0,      1.0,      0],
              [phi[0], phi[1],   phi[2], phi[3]],
              [ br[0],  br[1],    br[2],  br[3]]]

    return L1_mat
    
    
    
    
